# v0.0.4 (2019-12-20)

* Set target architecture in `client_info` ([#603](https://github.com/mozilla/glean/pull/603))

# v0.0.3 (2019-12-19)

* **Breaking Change**: Stub out client info values and provide a way to set app version information ([#592](https://github.com/mozilla/glean/pull/592))

# v0.0.2 (2019-12-09)

* Removed glean-ffi dependency in favor of implementing state in this crate

# v0.0.1 (2019-12-05)

First release of the Glean Rust API preview.
